package info.kwarc.mmt.isabelle_dummy

import info.kwarc.mmt.api._
import archives._
import documents._


class Importer extends archives.Importer
{
  def fail: Nothing =
    throw new RuntimeException("Undefined Isabelle: MMT build requires property \"isabelle.root\" or isabelle env")
  def key: String = fail
  def inExts: List[String] = fail
  def importDocument(bt: BuildTask, index: Document => Unit) : BuildResult = fail
}
