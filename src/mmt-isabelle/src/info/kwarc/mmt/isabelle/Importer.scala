package info.kwarc.mmt.isabelle

import info.kwarc.mmt.api._
import archives._
import symbols._
import modules._
import documents._
import opaque._
import objects._
import utils._


class Importer extends archives.Importer
{
  val key = "isabelle-omdoc"
  def inExts = List("thy")


  /* logging */

  val logger = new isabelle.Logger { def apply(msg: => String) { log(msg) } }

  val progress = new isabelle.Progress
  {
    override def echo(msg: String) { log(msg) }
    override def theory(session: String, theory: String)
    { log(isabelle.Progress.theory_message(session, theory)) }
  }


  /* Isabelle system environment */

  lazy val options: isabelle.Options = isabelle.Options.init()

  lazy val session_options: isabelle.Options =
    isabelle.Dump.make_options(options, isabelle.Dump.known_aspects)

  lazy val session_deps: isabelle.Sessions.Deps =
    isabelle.Sessions.load_structure(session_options).
      selection_deps(isabelle.Sessions.Selection.all, progress = progress)


  /* PIDE session */

  private var _session: Option[isabelle.Thy_Resources.Session] = None

  def session: isabelle.Thy_Resources.Session =
    _session.getOrElse(isabelle.error("No Isabelle/PIDE session"))

  override def start(args: List[String]): Unit =
  {
    super.start(args)

    isabelle.Isabelle_System.init()

    val include_sessions = session_deps.sessions_structure.imports_topological_order

    _session =
      Some(isabelle.Thy_Resources.start_session(session_options,
        isabelle.Isabelle_System.getenv("ISABELLE_LOGIC"),
        include_sessions = include_sessions, progress = progress, log = logger))
  }

  override def destroy
  {
    session.stop()
    _session = None
    super.destroy
  }


  /* import theory file */

  def importDocument(bt: BuildTask, index: Document => Unit) : BuildResult =
  {
    /* document */

    val doc = new Document(bt.narrationDPath, root = true)
    controller add doc

    val thy_node =
      session.resources.import_name(isabelle.Sessions.DRAFT, "",
        bt.inFile.canonical.stripExtension.getPath)

    val thy = Isabelle.make_theory(thy_node)
    controller add thy
    controller add MRef(doc.path, thy.path)

    val thy_text = isabelle.File.read(bt.inFile)
    val thy_text_output = isabelle.Symbol.decode(thy_text.replace(' ', '\u00a0'))
    controller.add(new OpaqueText(thy.asDocument.path, OpaqueText.defaultFormat, StringFragment(thy_text_output)))


    /* theory status */

    val theories = List(thy_node.path.split_ext._1.implode)
    val theories_result = session.use_theories(theories, progress = progress)

    val thy_status = theories_result.nodes.collectFirst({ case (name, status) if name == thy_node => status })


    /* theory exports */

    if (thy_status.isDefined && thy_status.get.ok) {
      val snapshot = theories_result.snapshot(thy_node)
      val provider = isabelle.Export.Provider.snapshot(snapshot)

      for (c <- isabelle.Export_Theory.read_types(provider)) {
        controller add Constant(thy.toTerm, LocalName("type." + c.entity.name), Nil, None, None, None)
      }

      for (c <- isabelle.Export_Theory.read_consts(provider)) {
        controller add Constant(thy.toTerm, LocalName("const." + c.entity.name), Nil, None, None, None)
      }

      for (c <- isabelle.Export_Theory.read_facts(provider)) {
        controller add Constant(thy.toTerm, LocalName("fact." + c.entity.name), Nil, None, None, None)
      }
    }


    /* result */

    index(doc)
    BuildResult.fromImportedDocument(doc)
  }
}
