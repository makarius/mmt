package info.kwarc.mmt.isabelle

import info.kwarc.mmt.api._
import modules._
import objects._
import utils._

object Isabelle
{
  val logic_base = DPath(URI("https", "isabelle.in.tum.de") / "logic")

  def make_theory(name: isabelle.Document.Node.Name): DeclaredTheory =
  {
    val mod = logic_base ? name.theory
    Theory.empty(mod.doc, mod.name, Some(mod))
  }

  val pure = logic_base ? "Pure"

  object Fun {
    val path = pure ? "fun"
    def apply(from: List[Term], to: Term) = OMA(OMS(path), from ::: List(to))
  }

  object Lambda {
    val path = pure ? "lambda"
    def apply(bindings: Context, body: Term) = OMBIND(OMS(path), bindings, body)
  }

  object Apply {
    val path = pure ? "apply"
    def apply(fun: Term, arg: List[Term]) = OMA(OMS(path), fun::arg)
  }

  object Prop {
    val path = pure ? "prop"
    def apply() = OMS(path)
  }

  object Forall {
    val path = pure ? "forall"
    def apply(name: String, tp: Term, body: Term) = OMBIND(OMS(path), Context(VarDecl(LocalName(name), tp)), body)
  }

  object Implies {
    val path = pure ? "implies"
    def apply(left: List[Term], right: Term) = OMA(OMS(path), left ::: List(right))
  }

  object Equal {
    val path = pure ? "equal"
    def apply(tp: Term, left: Term, right: Term) = OMA(OMS(path), List(tp, left, right))
  }
}
